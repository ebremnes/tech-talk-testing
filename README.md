# README #

### Pruebas de integración y cobertura de código con su aplicación Java Spring Boot ###

- A Spring boot app & TDD
    - https://spring.io/projects/spring-boot
    - http://agiledata.org/essays/tdd.html
- Cyclomatic Complexity 
- Code Coverage
- Java vrs 11 & JUnit 5
- JUnit The Platform
- Junit Annotations
    - https://junit.org/junit5/docs/current/user-guide/#writing-tests-annotations
- Lambda expressions
    - https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
- Assumptions 
    - https://junit.org/junit5/docs/current/user-guide/#writing-tests-assumptions
- Exceptions
    - https://www.baeldung.com/junit-assert-exception
- Suites
    - https://junit.org/junit5/docs/current/user-guide/#running-tests-junit-platform-runner-test-suite
- Dynamic testing with streams
- Mockito extension
- Mockito mock object injection
