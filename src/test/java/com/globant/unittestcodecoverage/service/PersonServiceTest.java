package com.globant.unittestcodecoverage.service;

import com.globant.unittestcodecoverage.model.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class PersonServiceTest {

    @InjectMocks
    PersonService personService;

    @Mock
    Person person;

    @BeforeEach
    void init() {
        personService = new PersonService();
    }

    @Test
    void setup() {
        person = personService.build(45, "Ivette");
        assertNotNull(person.getName().charAt(0));
    }

}