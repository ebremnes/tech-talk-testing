package com.globant.unittestcodecoverage.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


@RunWith(MockitoJUnitRunner.class)
class PersonTest {


    @Mock
    List<String> mockList = Mockito.mock(List.class);
    private Person person;

    @BeforeEach
    void setUp() {
        person = Person.builder()
                .name("Me")
                .age(40)
                .build();
    }

    @Test
    void MeExists() {
        //normal
        assertFalse(person.getName().isEmpty());
        //by group using Lambda
        assertAll("person", () -> assertFalse(person.getName().isEmpty()));
    }

    //Fail test - add logic
    @Test
    void ShouldFailWhenNoSurname() throws NoSuchFieldException {
        Field surname = person.getClass().getDeclaredField("surname");
    }

    @Test
    void MeHasAffixedValue() {
        assertEquals("Me", person.getName());
    }

    @Test
    void area() {
        mockList.add("one");
        Mockito.verify(mockList).add("one");
        assertEquals(0, mockList.size());
        System.out.println((mockList.size()));
        //mock the list size
        Mockito.when(mockList.size()).thenReturn(100);
        assertEquals(100, mockList.size());
        System.out.println((mockList.size()));
    }

    @Test
    void assumptions() {
        assumeFalse(mockList.isEmpty());
        assertEquals(1, 2 - 1);
        assumeTrue(getClass().getName().contains("Person"));
    }
}