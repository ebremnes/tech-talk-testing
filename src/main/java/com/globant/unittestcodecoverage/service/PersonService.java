package com.globant.unittestcodecoverage.service;

import com.globant.unittestcodecoverage.model.Person;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    public Person build(int age, String name) {
        return Person.builder()
                .age(age)
                .name(name)
                .build();
    }
}
