package com.globant.unittestcodecoverage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitTestCodeCoverageApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnitTestCodeCoverageApplication.class, args);
    }

}
